from flask import Flask, request
from werkzeug.utils import secure_filename
from time import time
import os
import numpy as np
import tensorflow as tf
from tensorflow import keras

app = Flask(__name__)

classes = ['Aluminium', 'Carton', 'Glass', 'Organic Waste', 'Other Plastics', 'Paper and Cardboard', 'Plastic', 'Textiles', 'Wood']
recycle = ['Aluminium', 'Carton', 'Glass', 'Other Plastics', 'Paper and Cardboard', 'Plastic', 'Textiles', 'Wood']
non_recycle = ['Organic Waste']
model = keras.models.load_model(f"{os.getcwd()}/WasteClassificationModel.h5")

def load_image(img_path):
    img = keras.preprocessing.image.load_img(img_path, target_size=(256, 256))
    img_tensor = keras.preprocessing.image.img_to_array(img)
    img_tensor = tf.expand_dims(img_tensor, axis=0)

    return img_tensor


@app.route("/image/upload", methods=['POST'])
def detect():
    upload_path = f'{os.getcwd()}/uploads'
    if (os.path.exists(upload_path) == False):
        os.mkdir(upload_path)

    file = request.files['file']
    file_path = f'{upload_path}/{secure_filename(f"{file.filename}-{time()}")}'
    file.save(file_path)
    try:
        if (model is not None):
            result = model.predict(load_image(file_path))
            tmp = classes[np.argmax(result)]
            print(result, tmp)
            return tmp in recycle and "1" or "0"
        else:
            return "0"
    finally:
        os.remove(file_path)

@app.route('/')
def hello_world():
    return "Hello World"
